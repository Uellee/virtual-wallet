package ru.tomsk.dea.vcash.presentation.export

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.databinding.ActivityExportBinding
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.presentation.BaseActivity
import ru.tomsk.dea.vcash.presentation.ViewModelFactory

class ExportActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_export)

        val binding = DataBindingUtil
                .setContentView<ActivityExportBinding>(this, R.layout.activity_export)

        binding.vm = ViewModelProviders.of(
                this,
                ViewModelFactory(ComponentOwner.uiComponent)
        ).get(ExportViewModel::class.java)
    }
}
