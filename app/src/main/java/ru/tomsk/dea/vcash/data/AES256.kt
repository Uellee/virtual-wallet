package ru.tomsk.dea.vcash.data

import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * Created by DEA on 8/27/17.
 * Handles encrypting data
 */
class AES256 {
    companion object {
        //MB in would be set as a secret
        private val IV: ByteArray = "In aqua sanitas.".toByteArray()

        fun encrypt(key: ByteArray, plainText: ByteArray): ByteArray {
            return apply(key, plainText, Cipher.ENCRYPT_MODE)
        }

        fun decrypt(key: ByteArray, encryptedText: ByteArray): ByteArray {
            return apply(key, encryptedText, Cipher.DECRYPT_MODE)
        }

        private fun apply(key: ByteArray, inputBytes: ByteArray, encryptMode: Int): ByteArray {
            val ivParameterSpec = IvParameterSpec(IV)
            val secretKeySpec = SecretKeySpec(key, "AES")
            val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
            cipher.init(encryptMode, secretKeySpec, ivParameterSpec)

            return cipher.doFinal(inputBytes)
        }
    }
}