package ru.tomsk.dea.vcash.data.rewallet

import ru.tomsk.dea.vcash.domain.model.Transaction

/**
 * Created by DEA on 8/27/17.
 * JSON format exporter
 */
class _JW {
    lateinit var _id: String
    lateinit var transactions: List<Transaction>
}