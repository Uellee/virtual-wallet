package ru.tomsk.dea.vcash.presentation.views

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.presentation.layoutInflater
import java.util.*

/**
 * Created by uellee on 7/30/17.
 * Displays 10 numerical buttons and backspace button with input of six digits indicator with text tip
 * On six digits enter calls onPinWasChangedListener's onPinWasChanged
 *
 * Contains following binding adapters:
 * onPinChangesListener: PinPadView.OnPinWasChangedListener, tipText, isWarning
 */
class PinPadView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet?, defAttrStyle: Int = 0) : FrameLayout(context, attrs, defAttrStyle) {

    private val pin = LinkedList<Int>()

    private val tipTextView by lazy { findViewById<TextView>(R.id.tv_tip) }

    private val indicatorList by lazy {
        arrayOf(R.id.ind_1num, R.id.ind_2num, R.id.ind_3num,
                R.id.ind_4num, R.id.ind_5num, R.id.ind_6num)
                .map { findViewById<InputIndicatorView>(it) }
    }

    var onPinWasChangedListener: OnPinWasChangedListener? = null

    var isWarningTip = false

    var currentTipText: String = ""
        set(value) {
            field = value
            setTipText(value)
        }

    var clearOnEnter = false

    private fun initButtons() {
        arrayOf(R.id.b_0, R.id.b_1, R.id.b_2, R.id.b_3, R.id.b_4,
                R.id.b_5, R.id.b_6, R.id.b_7, R.id.b_8, R.id.b_9)
                .map { findViewById<View>(it) }
                .forEachIndexed { i, view ->
                    view.setOnClickListener {
                        if (pin.size < 6) {
                            pin.add(i)
                            indicatePinChanged()
                        }
                    }
                }

        findViewById<View>(R.id.b_remove).setOnClickListener {
            if (pin.isNotEmpty()) {
                pin.removeLast()
                indicatePinChanged()
            }
        }
    }

    private fun indicatePinChanged() {
        for (i in 0..5) indicatorList[i].isPositive = i < pin.size
        if (pin.size == 6) onPinWasChangedListener?.onPinWasChanged(pin.value)
        if (clearOnEnter and (pin.size == 6)) {
            pin.clear()
            indicatePinChanged()
        }
    }

    private fun setTipText(s: String) {
        tipTextView.text = s
        if (isWarningTip) tipTextView.setTextColor(ContextCompat.getColor(context, R.color.colorWarning))
        else tipTextView.setTextColor(ContextCompat.getColor(context, R.color.colorTip))
    }

    interface OnPinWasChangedListener {
        fun onPinWasChanged(pin: Int)
    }

    private val LinkedList<Int>.value: Int
        get() {
            var v: Int = 0
            forEach { v = v * 10 + it }
            return v
        }

    init {
        context.layoutInflater.inflate(R.layout.numeric_pad, this)
        initButtons()
    }
}