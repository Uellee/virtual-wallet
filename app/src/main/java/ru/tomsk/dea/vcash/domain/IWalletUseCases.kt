package ru.tomsk.dea.vcash.domain

import io.reactivex.Observable
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Transaction

interface IWalletUseCases {
    val transactionsStream: Observable<Transaction>
    val observableStatus: Observable<Cash>

    fun onApplyTransaction(payee: String, amount: Int): Boolean
    fun onCreateTransaction()
    fun onImportWallet()
    fun onRemoveWallet()
    fun lockWallet()
}