package ru.tomsk.dea.vcash.di.ui

import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import ru.tomsk.dea.vcash.domain.*
import java.util.concurrent.Executor
import javax.inject.Inject

@Module
class WalletUseCasesModule @Inject constructor() {
    @Provides
    fun walletInteractor(
            repository: IRepository,
            executor: Executor,
            router: IRouter,
            permissionsManager: IPermissionsManager
    ): IWalletUseCases =
            WalletInteractor(repository, executor, router, permissionsManager)

    @Provides
    fun lockInteractor(repository: IRepository, router: IRouter): ILockUseCases =
            LockInteractor(repository, router)

    @Provides
    fun exportInteractor(
            repository: IRepository,
            permissionsManager: IPermissionsManager,
            scheduler: Scheduler,
            context: Context
    ): IExportUseCases = ExportInteractor(repository, permissionsManager, scheduler, context)
}