package ru.tomsk.dea.vcash.presentation.transactionslist

import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.domain.IWalletUseCases
import javax.inject.Inject

class ToolbarViewModel @Inject constructor(
        private val interactor: IWalletUseCases
) : ViewModel(), Toolbar.OnMenuItemClickListener {
    private val eventHandler = ComponentOwner.uiComponent.uiEventHandler()

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        item ?: return false

        when (item.itemId) {
            R.id.mi_import -> interactor.onImportWallet()
            R.id.mi_outgoing_transaction -> interactor.onCreateTransaction()
            R.id.mi_clear_wallet -> interactor.onRemoveWallet()
            R.id.mi_lock_wallet -> interactor.lockWallet()
        }

        return true
    }
}

@BindingAdapter("itemSelectedListener")
fun setListener(toolbar: Toolbar, listener: Toolbar.OnMenuItemClickListener) {
    toolbar.setOnMenuItemClickListener(listener)
}
