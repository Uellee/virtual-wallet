package ru.tomsk.dea.vcash.presentation.views

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.presentation.layoutInflater

/**
 * Created by uellee on 7/30/17.
 * Indicates two possible conditions
 * Is controlled by isPositive property
 */
class InputIndicatorView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : FrameLayout(context, attrs, defStyleAttr) {

    private val indicatorImage: ImageView
    private val positive_image = R.drawable.circle
    private val negative_image = R.drawable.circle_outline

    var isPositive = false
        set(value) {
            indicatorImage.setImageResource(
                    if (value) positive_image
                    else negative_image
            )
            field = value
        }

    init {
        context.layoutInflater.inflate(R.layout.indicator_control, this)
        indicatorImage = findViewById<AppCompatImageView>(R.id.i_indicator)
//        indicatorImage.setColorFilter(R.color.colorPrimaryDark)
    }

}