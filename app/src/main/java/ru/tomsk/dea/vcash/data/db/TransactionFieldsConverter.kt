package ru.tomsk.dea.vcash.data.db

import android.arch.persistence.room.TypeConverter
import ru.tomsk.dea.vcash.domain.model.Currency
import java.util.*

/**
 * Created by DEA on 8/21/17.
 * Converts data encapsulated by Transaction class
 */
class TransactionFieldsConverter {
    @TypeConverter
    fun dateToLong(date: Date) = date.time

    @TypeConverter
    fun longToDate(long: Long) = Date(long)

    @TypeConverter
    fun currencyToInt(currency: Currency) = currency.toInt()

    @TypeConverter
    fun intToCurrency(value: Int) = Currency.convert(value)
}