package ru.tomsk.dea.vcash.domain

import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by DEA on 8/26/17.
 * Base class of wallet interactor
 */
abstract class BaseInteractor(
        repository: IRepository,
        private val router: IRouter
) {
    init {
        repository.isLocked
                .filter { it }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { router.showLockFragment() }
    }
}