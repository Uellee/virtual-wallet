package ru.tomsk.dea.vcash.domain

import android.net.Uri
import io.reactivex.Observable

interface IExportUseCases {
    val keyCheckObservable: Observable<Boolean>

    fun restartExport(uri: Uri)
    fun startExport(uri: Uri)
    fun checkKey(key: Int)
}