package ru.tomsk.dea.vcash.presentation.outgoingtransaction


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.databinding.FragmentOutcomingTransactionBinding
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.presentation.ViewModelFactory

class OutGoingTransactionFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentOutcomingTransactionBinding>(
                inflater, R.layout.fragment_outcoming_transaction, container, false)
        val factory = ViewModelFactory(ComponentOwner.uiComponent)

        binding.vm = ViewModelProviders.of(this, factory).get(OutGoingTransactionViewModel::class.java)

        return binding.root
    }

}// Required empty public constructor
