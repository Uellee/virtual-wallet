package ru.tomsk.dea.vcash.di.app

import dagger.Binds
import dagger.Module
import ru.tomsk.dea.vcash.data.Repository
import ru.tomsk.dea.vcash.domain.IRepository

/**
 * Created by DEA on 8/14/17.
 * provides interactor to domain layer
 */

@Module
interface DataModule {
    @Binds fun provideRepository(repository: Repository): IRepository
}