package ru.tomsk.dea.vcash.domain

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import ru.tomsk.dea.vcash.data.Repository
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by DEA on 8/25/17.
 * Tracks user activeness
 */
@Singleton
class TrackingInteractor @Inject constructor(private val repository: Repository) : IActionsTracker {

    private var disposable: Disposable? = null

    override fun onAction() {

        repository.isLocked.filter { !it }.subscribe {
            disposable?.dispose()
            disposable = Observable.timer(1, TimeUnit.MINUTES).subscribe {
                repository.lock(true)
            }
        }
    }
}