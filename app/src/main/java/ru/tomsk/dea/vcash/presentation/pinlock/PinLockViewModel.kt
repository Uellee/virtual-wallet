package ru.tomsk.dea.vcash.presentation.pinlock

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.tomsk.dea.vcash.domain.ILockUseCases
import ru.tomsk.dea.vcash.domain.IRouter
import ru.tomsk.dea.vcash.presentation.set
import javax.inject.Inject

class PinLockViewModel @Inject constructor(
        private val interactor: ILockUseCases,
        private val router: IRouter
) : ViewModel() {

    val tip: ObservableField<String> = ObservableField("")
    var warning: ObservableBoolean = ObservableBoolean(false)

    init {
        interactor.wallet
                .observeOn(AndroidSchedulers.mainThread())
                .isEmpty
                .filter { it }
                .subscribe {
                    router.showGeneratePinFragment()
                }
    }

    fun checkPin(pin: Int) {
        interactor.checkPin(pin)
                .observeOn(AndroidSchedulers.mainThread())
                .filter { success -> !success }
                .subscribe { onFailedPinCheck() }
    }

    private fun onFailedPinCheck() {
        warning set true
        tip set "Invalid pin"
    }

}