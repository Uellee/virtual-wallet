package ru.tomsk.dea.vcash.presentation.pinlock

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.databinding.FragmentLockBinding
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.presentation.ViewModelFactory

class LockFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentLockBinding>(
                inflater, R.layout.fragment_lock, container, false)
        val viewModelFactory = ViewModelFactory(ComponentOwner.uiComponent)

        val vm = ViewModelProviders.of(this, viewModelFactory).get(PinLockViewModel::class.java)
        binding.vm = vm

        return binding.root
    }

    override fun onSaveInstanceState(outState: Bundle?) {
//        super.onSaveInstanceState(outState)
    }
}
