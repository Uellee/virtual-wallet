package ru.tomsk.dea.vcash.di.app

import dagger.Component
import ru.tomsk.dea.vcash.di.ui.PermissionsModule
import ru.tomsk.dea.vcash.di.ui.RouterModule
import ru.tomsk.dea.vcash.di.ui.UIComponent
import ru.tomsk.dea.vcash.presentation.WalletActivity
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        DataModule::class,
        SchedulerModule::class,
        DBModule::class,
        TrackingModule::class)
)
interface AppComponent {
    fun inject(walletActivity: WalletActivity)
    fun plusUIComponent(
            permissionsModule: PermissionsModule,
            routerModule: RouterModule
    ): UIComponent
}