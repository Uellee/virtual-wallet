package ru.tomsk.dea.vcash.presentation

import android.os.Bundle
import android.view.MotionEvent
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.domain.IActionsTracker
import javax.inject.Inject

class WalletActivity : BaseActivity() {

    @Inject lateinit var interactor: IActionsTracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ComponentOwner.appComponent.inject(this)

        setContentView(R.layout.activity_wallet)

        val router = Router(supportFragmentManager)

        router.showLockFragment()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        interactor.onAction()
        return super.dispatchTouchEvent(ev)
    }
}