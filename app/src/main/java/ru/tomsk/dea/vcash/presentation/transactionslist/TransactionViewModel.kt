package ru.tomsk.dea.vcash.presentation.transactionslist

import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v7.widget.AppCompatImageView
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Currency
import ru.tomsk.dea.vcash.domain.model.Transaction
import ru.tomsk.dea.vcash.presentation.set

/**
 * Created by DEA on 8/2/17.
 * View model of transaction item
 */
class TransactionViewModel : ViewModel() {
    val date: ObservableField<String> = ObservableField("")
    val cash: ObservableField<Cash> = ObservableField(Cash(0, Currency.RUB))
    val isIncoming: ObservableBoolean = ObservableBoolean(true)

    infix fun set(transaction: Transaction) {
        date set transaction.date.toString()
        cash set transaction.cash
        isIncoming set transaction.isIncoming
    }

    fun inflate(transaction: Transaction): TransactionViewModel {
        this set transaction
        return this
    }

    override fun equals(other: Any?): Boolean {
        if (other === null) return false
        if (other !is TransactionViewModel) return false

        return date == other.date && cash == other.cash && isIncoming == other.isIncoming
    }

    override fun hashCode(): Int {
        var result = date.hashCode()
        result = 31 * result + cash.hashCode()
        result = 31 * result + isIncoming.hashCode()
        return result
    }
}

@BindingAdapter("isIncoming")
fun setTransactionTypeDrawable(v: AppCompatImageView, isIncoming: Boolean) {
    v.setImageResource(
            if (isIncoming) R.drawable.ic_arrow_left
            else R.drawable.ic_arrow_right
    )
}