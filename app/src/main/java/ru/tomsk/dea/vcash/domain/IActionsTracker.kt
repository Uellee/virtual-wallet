package ru.tomsk.dea.vcash.domain

/**
 * Created by DEA on 8/25/17.
 * provides methods to perform on interaction
 */
interface IActionsTracker {
    fun onAction()
}