package ru.tomsk.dea.vcash.data.rewallet

import android.net.Uri
import android.os.Environment
import java.io.File

/**
 * Created by DEA on 8/27/17.
 * Handles creating a file with wallet data
 */
class FileImporter {
    companion object {

        private val EXTENSION = "evw"

        fun createFile(name: String, content: ByteArray) {
            getFile("$name.$EXTENSION").writeBytes(content)
        }

        fun getData(uri: Uri) = File(uri.path).readBytes()

        private fun getFile(name: String) = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
                name
        )
    }
}