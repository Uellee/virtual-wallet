package ru.tomsk.dea.vcash.presentation.walletgeneration

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import ru.tomsk.dea.vcash.domain.ILockUseCases
import ru.tomsk.dea.vcash.presentation.set
import javax.inject.Inject

/**
 * Created by DEA on 8/10/17.
 * View model of generating new wallet
 */
class WalletGenerationViewModel
@Inject constructor(
        private val interactor: ILockUseCases
) : ViewModel() {

    private val CREATE_PIN_DESCRIPTION = "Enter your new pin"
    private val REPEAT_PIN_DESCRIPTION = "Confirm your new pin"

    val description = ObservableField(CREATE_PIN_DESCRIPTION)
    val warning = ObservableBoolean(false)
    val tip = ObservableField("")
    val clearOnEnter = ObservableBoolean(true)

    private var newPin = -1
        set(value) {
            clearOnEnter set (value == -1)
            field = value
        }

    fun onPinEntered(pin: Int) = when (newPin) {
        -1 -> onCreatingNewPin(pin)
        pin -> onSuccessfulCreation()
        else -> onUnmatchedPins()
    }

    private fun onSuccessfulCreation() {
        interactor.onWalletCreation(newPin)
    }

    private fun onCreatingNewPin(pin: Int) {
        newPin = pin
        description set REPEAT_PIN_DESCRIPTION
    }

    private fun onUnmatchedPins() {
        warning set true
        tip set "Pin does not match, try again"
    }

}