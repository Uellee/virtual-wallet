package ru.tomsk.dea.vcash.data.rewallet

import com.google.gson.Gson
import ru.tomsk.dea.vcash.domain.model.Transaction

/**
 * Created by DEA on 8/27/17.
 * Creates json string from in-app data for import goal
 */
class JsonWallet {
    private val gson = Gson()
    private lateinit var id: String
    private val transactionsList = ArrayList<Transaction>()

    val jsonResult: String
        get() {
            val jw = _JW()

            jw._id = id
            jw.transactions = transactionsList

            return gson.toJson(jw)
        }

    fun addWallet(walletId: String) {
        id = walletId
    }

    fun addTransaction(transaction: Transaction) = transactionsList.add(transaction)

    companion object {
        fun createJson(_id: String, transactionsList: List<Transaction>): String {
            val gson = Gson()
            val id = gson.toJson(_id)
            val transactions = gson.toJson(transactionsList)
            return id + transactions
        }

        /*
        * Throws com.google.gson.JsonSyntaxException if json is not a valid representation for _JW
        * */
        fun restore(json: String): _JW = Gson().fromJson(json, _JW::class.java)
    }
}