package ru.tomsk.dea.vcash.presentation

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import ru.tomsk.dea.vcash.di.ui.UIComponent
import ru.tomsk.dea.vcash.presentation.export.ExportViewModel
import ru.tomsk.dea.vcash.presentation.outgoingtransaction.OutGoingTransactionViewModel
import ru.tomsk.dea.vcash.presentation.pinlock.PinLockViewModel
import ru.tomsk.dea.vcash.presentation.transactionslist.ToolbarViewModel
import ru.tomsk.dea.vcash.presentation.transactionslist.WalletViewModel
import ru.tomsk.dea.vcash.presentation.walletgeneration.WalletGenerationViewModel
import java.io.Serializable

/**
 * Created by DEA on 8/8/17.
 * creates app used viewModels with dagger
 */
class ViewModelFactory(val uiComponent: UIComponent) : ViewModelProvider.Factory, Serializable {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when (modelClass) {
        PinLockViewModel::class.java ->
            uiComponent.pinLockViewModel() as T

        WalletViewModel::class.java ->
            uiComponent.walletViewModel() as T

        WalletGenerationViewModel::class.java ->
            uiComponent.walletGenerationViewModel() as T

        OutGoingTransactionViewModel::class.java ->
            uiComponent.outGoingTransactionViewModel() as T

        ToolbarViewModel::class.java ->
            uiComponent.toolbarViewModel() as T

        ExportViewModel::class.java ->
            uiComponent.exportViewModel() as T

        else -> modelClass.newInstance()
    }
}