package ru.tomsk.dea.vcash.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import ru.tomsk.dea.vcash.domain.model.Transaction

@Database(entities = arrayOf(Transaction::class, Status::class), version = 2, exportSchema = false)
abstract class WalletDatabase : RoomDatabase() {
    abstract fun transactionDao(): TransactionDao
    abstract fun statusDao(): StatusDao
}