package ru.tomsk.dea.vcash.domain.model


/**
 * Created by DEA on 8/4/17.
 * contains information about cash sum & used currency
 */

fun Cash(sum: Int, currency: Currency) = Cash().apply {
    this.sum = sum
    this.currency = currency
}

class Cash {
    var sum: Int = 0
    lateinit var currency: Currency

    fun emitTransaction(transaction: Transaction) = Cash(sum + transaction.cash.sum, currency)

    override fun equals(other: Any?): Boolean {
        if (other === null) return false
        if (other !is Cash) return false

        return currency == other.currency && sum == other.sum
    }

    override fun hashCode(): Int {
        var result = sum
        result = 31 * result + currency.hashCode()
        return result
    }
}
