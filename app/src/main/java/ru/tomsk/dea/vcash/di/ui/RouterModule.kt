package ru.tomsk.dea.vcash.di.ui

import android.support.v4.app.FragmentManager
import dagger.Module
import dagger.Provides
import ru.tomsk.dea.vcash.domain.IRouter
import ru.tomsk.dea.vcash.presentation.Router

@Module
class RouterModule(val fragmentManager: FragmentManager) {
    @Provides
    fun getRouter(): IRouter = Router(fragmentManager)
}