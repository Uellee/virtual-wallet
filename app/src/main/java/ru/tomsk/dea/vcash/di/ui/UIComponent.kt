package ru.tomsk.dea.vcash.di.ui

import dagger.Subcomponent
import ru.tomsk.dea.vcash.presentation.UIEventHandler
import ru.tomsk.dea.vcash.presentation.export.ExportViewModel
import ru.tomsk.dea.vcash.presentation.outgoingtransaction.OutGoingTransactionViewModel
import ru.tomsk.dea.vcash.presentation.pinlock.PinLockViewModel
import ru.tomsk.dea.vcash.presentation.transactionslist.ToolbarViewModel
import ru.tomsk.dea.vcash.presentation.transactionslist.WalletViewModel
import ru.tomsk.dea.vcash.presentation.walletgeneration.WalletGenerationViewModel

@Subcomponent(modules = arrayOf(
        RouterModule::class,
        WalletUseCasesModule::class,
        PermissionsModule::class
))
interface UIComponent {
    fun pinLockViewModel(): PinLockViewModel
    fun walletViewModel(): WalletViewModel
    fun walletGenerationViewModel(): WalletGenerationViewModel
    fun outGoingTransactionViewModel(): OutGoingTransactionViewModel
    fun toolbarViewModel(): ToolbarViewModel
    fun exportViewModel(): ExportViewModel

    fun uiEventHandler(): UIEventHandler
}