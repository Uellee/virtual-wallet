package ru.tomsk.dea.vcash.presentation

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.di.ui.PermissionsModule
import ru.tomsk.dea.vcash.di.ui.RouterModule

abstract class BaseActivity : AppCompatActivity(), PermissionsGrantedListener {

    private val permissionsSubject = PublishSubject.create<Int>()
    override val permissionsStream: Observable<Int>
        get() = permissionsSubject.hide()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        ComponentOwner.uiComponent = ComponentOwner.appComponent
                .plusUIComponent(
                        PermissionsModule(this),
                        RouterModule(supportFragmentManager)
                )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionsSubject.onNext(grantResults[0])
    }
}

interface PermissionsGrantedListener {
    val permissionsStream: Observable<Int>
}