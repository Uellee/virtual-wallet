package ru.tomsk.dea.vcash.domain.model

/**
 * Created by uellee on 7/31/17.
 * Contains possible currencies in future versions (m.b. future creation by id)
 * & appropriating drawable resources
 */
enum class Currency(val value: Int = 0) {
    RUB(0), USD(1);

    companion object {
        val MAX_VALUE = 1

        fun convert(value: Int) = when (value) {
            0 -> RUB
            1 -> USD

            else -> RUB
        }
    }

    fun toInt() = value
}