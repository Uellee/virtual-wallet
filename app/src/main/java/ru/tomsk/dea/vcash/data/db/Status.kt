package ru.tomsk.dea.vcash.data.db

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import ru.tomsk.dea.vcash.domain.model.Cash

/**
 * Created by DEA on 8/22/17.
 * Status entity
 */
@Entity(tableName = "status")
@TypeConverters(TransactionFieldsConverter::class)
class Status {
    //    Single record
    @PrimaryKey
    var id = 0
    @Embedded
    lateinit var cash: Cash
}