package ru.tomsk.dea.vcash.presentation.export

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.tomsk.dea.vcash.domain.IExportUseCases
import ru.tomsk.dea.vcash.presentation.set
import javax.inject.Inject

class ExportViewModel @Inject constructor(
        private val interactor: IExportUseCases,
        activity: Activity
) : ViewModel() {
    val tip: ObservableField<String> = ObservableField("")
    var warning: ObservableBoolean = ObservableBoolean(false)

    private val keyDisposable: Disposable

    init {
        keyDisposable = interactor.keyCheckObservable
                .observeOn(AndroidSchedulers.mainThread())
                .filter { passed -> !passed }
                .subscribe {
                    onFailedPinCheck()
                }

        interactor.startExport(activity.intent.data)
    }

    override fun onCleared() {
        super.onCleared()
        keyDisposable.dispose()
    }

    fun checkPin(pin: Int) = interactor.checkKey(pin)

    private fun onFailedPinCheck() {
        warning set true
        tip set "Invalid key"
    }
}