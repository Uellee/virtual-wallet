package ru.tomsk.dea.vcash.domain

import android.util.Log
import io.reactivex.Maybe
import io.reactivex.Single
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Currency
import ru.tomsk.dea.vcash.domain.model.Wallet
import javax.inject.Inject

/**
 * Created by DEA on 8/25/17.
 * use cases on lock state
 */
class LockInteractor @Inject constructor(
        private val repository: IRepository,
        private val router: IRouter
) : ILockUseCases {
    private val hashHandler = HashHandler()
    private val TAG = "#% LickInteractor"

    override val wallet: Maybe<Wallet> = repository.walletStream

    init {

        var id = ""
        repository.walletStream.subscribe { id = it.id }

        Log.d(TAG, "Current wallet: $id")
    }

    override fun checkPin(pin: Int): Single<Boolean> = repository.pinHashStream
            .map { hashHandler.checkPin(pin, it) }
            .doOnSuccess {
                if (it) {
                    router.showWalletFragment()
                    repository.lock(false)
                }
            }

    override fun onWalletCreation(pin: Int) {
        repository.createWallet(
                Wallet(hashHandler.generateString(), Cash(0, Currency.RUB)),
                hashHandler.createHash(pin)
        )

        repository.lock(false)

        router.showWalletFragment()
    }
}