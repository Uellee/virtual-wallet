package ru.tomsk.dea.vcash.presentation.views

import android.databinding.BindingAdapter
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Currency

/**
 * Created by DEA on 8/5/17.
 * Contains custom views binding adapters
 */

//AccountStatusView

@BindingAdapter("statusDescription")
fun setStatusDescription(v: AccountStatusView, description: String) {
    v.statusDescriptionTextView.text = description
}

@BindingAdapter("status")
fun setStatus(v: AccountStatusView, cash: Cash) {
    setCash(v.statusCashView, cash)
}

//CashView

@BindingAdapter("value")
fun setCash(cashView: CashView, cash: Cash) {
//    TODO установить ограничение высоты по размеру текста

    val currencyImgRes = when (cash.currency) {
        Currency.RUB -> R.drawable.currency_rub
        Currency.USD -> R.drawable.currency_usd
    }

    val p = cash.sum % 100
    val sum = StringBuilder().append(cash.sum / 100)
            .append(".")
            .append(if (p < 0) p * -1 else p)
            .toString()

    cashView.text = sum

    cashView.setCompoundDrawablesRelativeWithIntrinsicBounds(currencyImgRes, 0, 0, 0)

}

//PinPadView

@BindingAdapter("onPinChangesListener")
fun setOnPinChangedListener(pad: PinPadView, listener: PinPadView.OnPinWasChangedListener) {
    pad.onPinWasChangedListener = listener
}

@BindingAdapter("tipText")
fun setTipText(pad: PinPadView, tip: String) {
    pad.currentTipText = tip
}

@BindingAdapter("isWarning")
fun setWarning(pad: PinPadView, warning: Boolean) {
    pad.isWarningTip = warning
}

@BindingAdapter("clearOnEnter")
fun setClearOnEnter(pad: PinPadView, clear: Boolean) {
    pad.clearOnEnter = clear
}