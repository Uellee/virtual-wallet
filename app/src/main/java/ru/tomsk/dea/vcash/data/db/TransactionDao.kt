package ru.tomsk.dea.vcash.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import ru.tomsk.dea.vcash.domain.model.Transaction

@Dao
interface TransactionDao {
    @Query("SELECT * FROM transactions")
    fun loadTransactions(): Flowable<List<Transaction>>

    @Insert
    fun insert(transaction: Transaction)

    @Query("DELETE FROM transactions")
    fun clearTable()
}