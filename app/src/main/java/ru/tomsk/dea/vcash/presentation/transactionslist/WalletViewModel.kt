package ru.tomsk.dea.vcash.presentation.transactionslist

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.tomsk.dea.vcash.domain.IWalletUseCases
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Currency
import ru.tomsk.dea.vcash.presentation.set
import javax.inject.Inject

/**
 * Created by DEA on 7/31/17.
 * View model of base fragment,
 * contains information about existing transactions & current account observableStatus
 */
class WalletViewModel @Inject constructor(
        private val interactor: IWalletUseCases
) : ViewModel(), LifecycleObserver {
    val status: ObservableField<Cash> = ObservableField(Cash(0, Currency.RUB))
    val description: ObservableField<String> = ObservableField("Состояние счета:")
    val transactionsAdapter by lazy {
        TransactionsListAdapter()
    }

    private lateinit var disposableStatus: Disposable
    private lateinit var disposableTransactions: Disposable

    fun createOutGoingTransaction() {
        interactor.onCreateTransaction()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onViewStart() {
        disposableStatus = interactor.observableStatus
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    status set it
                }

        disposableTransactions = interactor.transactionsStream
                .map { TransactionViewModel().inflate(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    transactionsAdapter.addTransaction(it)
                }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onViewPause() {
        disposableStatus.dispose()
        disposableTransactions.dispose()

        transactionsAdapter.clear()
    }
}