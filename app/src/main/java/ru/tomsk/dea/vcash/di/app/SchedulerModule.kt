package ru.tomsk.dea.vcash.di.app

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

/**
 * Created by DEA on 8/17/17.
 * Provides scheduler
 */
@Module
class SchedulerModule {
    @Provides
    @Singleton
    fun provideScheduler(executor: Executor) = Schedulers.from(executor)

    @Provides
    @Singleton
    fun provideExecutor(): Executor = Executors.newSingleThreadExecutor()
}