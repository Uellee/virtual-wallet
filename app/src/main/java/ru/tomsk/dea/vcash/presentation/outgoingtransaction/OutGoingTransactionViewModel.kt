package ru.tomsk.dea.vcash.presentation.outgoingtransaction

import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.widget.EditText
import ru.tomsk.dea.vcash.domain.IWalletUseCases
import ru.tomsk.dea.vcash.presentation.set
import javax.inject.Inject

/**
 * Created by DEA on 8/12/17.
 * View model of outgoing transaction
 * Gets information from EditText fields
 */

class OutGoingTransactionViewModel
@Inject constructor(
        private val interactor: IWalletUseCases
) : ViewModel() {
    val amount = ObservableInt()
    val payee = ObservableField("")
    val message = ObservableField("")

    fun createTransaction() {
        if (!interactor.onApplyTransaction(payee.get(), amount.get()))
            showWarningMessage()
    }

    private fun showWarningMessage() {
        message set "Check fields values"
    }
}

@BindingAdapter("android:text")
fun setAmount(editText: EditText, amount: Int) {
    val string = editText.text.toString()
    val f = amount / 100.0

    if (string.isEmpty()) {
        editText.setText(f.toString())
        return
    }

    val currentAmount = (string.toFloat() * 100).toInt()

    if (currentAmount != amount) editText.setText(f.toString())
}

@InverseBindingAdapter(attribute = "android:text")
fun getAmount(editText: EditText): Int {
    if (editText.text.isEmpty()) return 0

    val s = editText.text.toString()

//    TODO check bug here
    return (s.toDouble() * 100).toInt()
}