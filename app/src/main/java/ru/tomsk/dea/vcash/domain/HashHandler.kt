package ru.tomsk.dea.vcash.domain

import org.hashids.Hashids

/**
 * Created by DEA on 8/17/17.
 * handles hash logic
 */
class HashHandler {
    private val SALT = "red hot chili salt & pepper"
    private val h = Hashids(SALT, 32)

    fun createHash(pin: Int) = h.encode(pin.toLong())!!

    fun generateString() = h.encode(System.currentTimeMillis() % Hashids.MAX_NUMBER)!!

    fun checkPin(pin: Int, hash: String): Boolean = hash == h.encode(pin.toLong())

}