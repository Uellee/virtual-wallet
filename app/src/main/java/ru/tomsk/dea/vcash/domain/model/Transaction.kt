package ru.tomsk.dea.vcash.domain.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import ru.tomsk.dea.vcash.data.db.TransactionFieldsConverter
import java.util.*

/**
 * Created by DEA on 8/2/17.
 * Contains transaction information
 */

fun Transaction(cash: Cash, party: String) = Transaction().apply {
    this.date = Date(System.currentTimeMillis())
    this.party = party
    this.cash = cash
}

@Entity(tableName = "transactions")
@TypeConverters(TransactionFieldsConverter::class)
class Transaction {

    lateinit var date: Date
    lateinit var party: String
    @Embedded
    lateinit var cash: Cash

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    val isIncoming: Boolean
        get() = cash.sum > 0

    override fun equals(other: Any?): Boolean {
        if (other === null) return false
        if (other !is Transaction) return false

        return date == other.date && party == other.party && cash == other.cash
    }

    override fun hashCode(): Int = 31 * party.hashCode() + date.hashCode()
}