package ru.tomsk.dea.vcash.presentation.transactionslist

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import ru.tomsk.dea.vcash.databinding.TransactionItemBinding

/**
 * Created by DEA on 8/2/17.
 *
 */
class TransactionViewHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    val binding: TransactionItemBinding = binding as TransactionItemBinding
}