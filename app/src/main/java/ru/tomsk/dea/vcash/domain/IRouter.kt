package ru.tomsk.dea.vcash.domain

/**
 * Created by DEA on 8/6/17.
 * Provides functions to manage basic used Fragments
 */
interface IRouter {
    fun showWalletFragment()
    fun showLockFragment()
    fun showGeneratePinFragment()
    fun showNewTransactionFragment()

    fun stopCurrentFragment()
}