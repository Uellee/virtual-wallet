package ru.tomsk.dea.vcash.data

import android.content.Context
import android.net.Uri
import android.util.Log
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import ru.tomsk.dea.vcash.data.db.Status
import ru.tomsk.dea.vcash.data.db.WalletDatabase
import ru.tomsk.dea.vcash.data.rewallet.FileImporter
import ru.tomsk.dea.vcash.data.rewallet.JsonWallet
import ru.tomsk.dea.vcash.data.rewallet._JW
import ru.tomsk.dea.vcash.domain.IRepository
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Currency
import ru.tomsk.dea.vcash.domain.model.Transaction
import ru.tomsk.dea.vcash.domain.model.Wallet
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by DEA on 8/14/17.
 * Handles data cache
 */
@Singleton
class Repository @Inject constructor(
        context: Context,
        private val scheduler: Scheduler,
        private val executor: Executor,
        singleDB: Single<WalletDatabase>
) : IRepository {

    private val TAG = "#% Repository"

    private var randomTransactionsDisposable: Disposable? = null
    private lateinit var exportData: ByteArray
    private val keySubject = PublishSubject.create<Boolean>()
    override val keyCheckObservable: Observable<Boolean>
        get() = keySubject.hide()

    private val isLockedSubject = BehaviorSubject.createDefault(false)

    override val isLocked: Observable<Boolean>
        get() = isLockedSubject.hide()

    private val transactionDao = singleDB.map { it.transactionDao() }.subscribeOn(scheduler)
    private val statusDao = singleDB.map { it.statusDao() }.subscribeOn(scheduler)

    private val walletPreferences = WalletPreferences(context)

    private lateinit var transactionsSubject: ReplaySubject<Transaction>
    override val transactionsStream: Observable<Transaction>
        get() = transactionsSubject.hide()

    private lateinit var statusSubject: BehaviorSubject<Cash>
    override val status: Observable<Cash>
        get() = statusSubject.hide()
    private var walletId: String? = walletPreferences.getWalletId()

    override val walletStream: Maybe<Wallet>
        get() = statusDao.observeOn(scheduler)
                .filter { walletId != null }
                .map { it.loadStatus().cash }
                .map { Wallet(walletId!!, it) }
    override val pinHashStream: Single<String>
        get() = Single.fromCallable { walletPreferences.getHash() }
                .subscribeOn(scheduler)

    init {
        initializeDBCache()
        launchRandomIncomingTransactions()
    }

    private fun initializeDBCache() {
        transactionsSubject = ReplaySubject.create<Transaction>()
        statusSubject = BehaviorSubject.create<Cash>()


        if (null != walletId) {

            Log.d(TAG, "current wallet: $walletId")
            transactionDao.observeOn(scheduler)
                    .flatMapPublisher { it.loadTransactions().take(1) }
                    .flatMap { Flowable.fromIterable(it) }
                    .subscribe { transactionsSubject.onNext(it) }

            statusDao.observeOn(scheduler)
                    .map { it.loadStatus() }
                    .map { it.cash }
                    .subscribe { cash -> statusSubject.onNext(cash) }
        }
    }

    override fun lock(state: Boolean) {
        isLockedSubject.onNext(state)

        if (state) randomTransactionsDisposable?.dispose()
        else launchRandomIncomingTransactions()
    }

    override fun createTransaction(transaction: Transaction) {

        val newStatus = statusSubject.value.emitTransaction(transaction)

        statusSubject.onNext(newStatus)

        statusDao.observeOn(scheduler)
                .subscribe { it ->
                    it.insert(Status().apply { cash = newStatus })
                    Log.d("#% Repository", "Updating status: ${newStatus.sum}")
                }

        transactionDao.observeOn(scheduler)
                .subscribe { it ->
                    it.insert(transaction)
                    Log.d("#% Repository", "Inserting new transaction: " +
                            "${transaction.cash.sum} ${transaction.party}")
                }

        transactionsSubject.onNext(transaction)
    }

    override fun createWallet(wallet: Wallet, hash: String, transactions: List<Transaction>?) {
        Log.d(TAG, "Creating wallet ${wallet.id}")
        walletPreferences.putNewWallet(wallet, hash)
        walletId = wallet.id

        val walletStatus = Status().apply { cash = wallet.status }
        statusDao.observeOn(scheduler)
                .subscribe { it -> it.insert(walletStatus) }

        statusSubject.onNext(wallet.status)

        transactions?.forEach {
            createTransaction(it)
        }

        launchRandomIncomingTransactions()
    }

    private fun launchRandomIncomingTransactions() {
        val random = Random()
        randomTransactionsDisposable = Observable.interval(10, TimeUnit.SECONDS, scheduler)
                .map { random.nextInt(100000) }
//                Set 30% chance
                .filter { it % 100 < 30 }
                .map { Cash(it, Currency.RUB) }
                .map { Transaction(it, "Random incoming transaction") }
                .subscribe { createTransaction(it) }
    }

    override fun import() {
        val jsonConstructor = JsonWallet()
        jsonConstructor.addWallet(walletId!!)

        transactionsSubject.map { jsonConstructor.addTransaction(it) }.subscribe()

        val key = walletPreferences.getHash().toByteArray()
        val encrypt = AES256.encrypt(key, jsonConstructor.jsonResult.toByteArray())

        FileImporter.createFile(walletId!!, encrypt)
    }

    override fun startExport(uri: Uri) {
        exportData = FileImporter.getData(uri)
    }

    override fun checkKey(key: String) {
        try {
            val decrypt = AES256.decrypt(key.toByteArray(), exportData)
            val jw = JsonWallet.restore(String(decrypt))
            completeExport(key, jw)
        } catch (e: Exception) {
            keySubject.onNext(false)
        }
    }

    private fun completeExport(hash: String, jw: _JW) {
        executor.execute {
            cleanCache()
            Log.d(TAG, "JW: ${jw._id} $hash")
            createWallet(Wallet(jw._id, Cash(0, Currency.RUB)), hash, jw.transactions)
        }

        keySubject.onNext(true)
    }

    override fun clean() {
        cleanCache()
        walletPreferences.clean()
        lock(true)
    }

    private fun cleanCache() {
        walletId = null

        executor.execute {
            statusDao.subscribe { dao -> dao.clearTable() }
            transactionDao.subscribe { dao -> dao.clearTable() }
        }

        initializeDBCache()
    }
}