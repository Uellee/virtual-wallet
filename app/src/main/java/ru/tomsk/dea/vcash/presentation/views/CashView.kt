package ru.tomsk.dea.vcash.presentation.views

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by DEA on 8/4/17.
 * visual representation of Cash class
 */
class CashView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : TextView(context, attrs, defStyleAttr)