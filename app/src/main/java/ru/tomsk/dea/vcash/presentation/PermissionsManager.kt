package ru.tomsk.dea.vcash.presentation

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import io.reactivex.Observable
import ru.tomsk.dea.vcash.domain.IPermissionsManager

/**
 * Created by DEA on 8/27/17.
 * Manages necessary permissions
 */
class PermissionsManager(private val activity: Activity) : IPermissionsManager {

    private val TAG = "#% Permissions manager"
    private val permissionsRequestResultObservable =
            (activity as PermissionsGrantedListener).permissionsStream

    override val WRITE_STORAGE_PERMISSION_GRANTED: Boolean
        get() = ContextCompat.checkSelfPermission(
                activity.baseContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

    override fun queryWriteStoragePermission(): Observable<Int> {

        if (!WRITE_STORAGE_PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    0
            )

        return permissionsRequestResultObservable
    }
}