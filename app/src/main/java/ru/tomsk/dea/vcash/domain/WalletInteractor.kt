package ru.tomsk.dea.vcash.domain

import android.content.pm.PackageManager
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Currency
import ru.tomsk.dea.vcash.domain.model.Transaction
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * Created by DEA on 8/3/17.
 * Generates fake data
 */
class WalletInteractor @Inject constructor(
        private val repository: IRepository,
        private val executor: Executor,
        private val router: IRouter,
        private val permissionsManager: IPermissionsManager
) : BaseInteractor(repository, router), IWalletUseCases {

    private val eventHandler = ComponentOwner.uiComponent.uiEventHandler()

    override val transactionsStream = repository.transactionsStream
    override val observableStatus = repository.status

    override fun onCreateTransaction() {
        router.showNewTransactionFragment()
    }

    override fun onApplyTransaction(payee: String, amount: Int): Boolean {

        if (amount <= 0) return false
//        if (payee.length != 32) return false

        executor.execute {
            val transaction = Transaction(Cash(-amount, Currency.RUB), payee)
            repository.createTransaction(transaction)
        }

        router.stopCurrentFragment()

        return true
    }

    override fun onImportWallet() {
        if (permissionsManager.WRITE_STORAGE_PERMISSION_GRANTED)
            executor.execute { repository.import() }
        else
            permissionsManager.queryWriteStoragePermission()
                    .map { it == PackageManager.PERMISSION_GRANTED }
                    .subscribe {
                        if (it) executor.execute { repository.import() }
                        else eventHandler.showToast("Can't create file without permission")
                    }
    }

    override fun onRemoveWallet() {
        repository.clean()
    }

    override fun lockWallet() {
        repository.lock(true)
    }
}
