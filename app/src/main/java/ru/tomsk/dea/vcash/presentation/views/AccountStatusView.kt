package ru.tomsk.dea.vcash.presentation.views

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.presentation.layoutInflater

/**
 * Created by uellee on 7/31/17.
 * Displays short description & account information: currency & account status
 */
class AccountStatusView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

    val statusDescriptionTextView: TextView
    val statusCashView: CashView

    init {
        val inflater = context.layoutInflater
        inflater.inflate(R.layout.account_status_control, this, true)

        statusDescriptionTextView = findViewById(R.id.tv_status_description)
        statusCashView = findViewById(R.id.cv_status)
    }

    fun setCurrency(img: Int) {
        statusCashView.setCompoundDrawablesWithIntrinsicBounds(img, 0, 0, 0)
    }
}