package ru.tomsk.dea.vcash.di

import ru.tomsk.dea.vcash.di.app.AppComponent
import ru.tomsk.dea.vcash.di.ui.UIComponent

/**
 * Created by DEA on 8/28/17.
 * Owns dagger components
 */
object ComponentOwner {
    lateinit var appComponent: AppComponent
    lateinit var uiComponent: UIComponent
}