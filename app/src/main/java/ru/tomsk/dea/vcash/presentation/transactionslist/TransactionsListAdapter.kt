package ru.tomsk.dea.vcash.presentation.transactionslist

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.tomsk.dea.vcash.databinding.TransactionItemBinding
import ru.tomsk.dea.vcash.presentation.layoutInflater

/**
 * Created by DEA on 8/2/17.
 * TransactionsList adapter, gets list of transactions, contains binding adapter for a recyclerView
 */
class TransactionsListAdapter : RecyclerView.Adapter<TransactionViewHolder>() {

    var transactionViewModels = ArrayList<TransactionViewModel>()

    fun addTransaction(transactionViewModel: TransactionViewModel) {
        transactionViewModels.add(0, transactionViewModel)
        notifyItemInserted(0)
    }

    fun clear() {
        transactionViewModels.clear()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.binding.vm = transactionViewModels[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val binding = TransactionItemBinding.inflate(parent.context.layoutInflater, parent, false)
        binding.vm = TransactionViewModel()

        return TransactionViewHolder(binding)
    }

    override fun getItemCount() = transactionViewModels.size

}

@BindingAdapter("adapter")
fun setAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
    recyclerView.adapter = adapter
}