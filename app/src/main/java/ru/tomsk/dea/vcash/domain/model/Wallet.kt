package ru.tomsk.dea.vcash.domain.model

/**
 * Created by DEA on 8/14/17.
 * wallet model
 */
data class Wallet(val id: String, val status: Cash)