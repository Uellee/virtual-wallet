package ru.tomsk.dea.vcash.di.app

import dagger.Binds
import dagger.Module
import ru.tomsk.dea.vcash.domain.IActionsTracker
import ru.tomsk.dea.vcash.domain.TrackingInteractor
import javax.inject.Singleton

@Module
interface TrackingModule {
    @Singleton
    @Binds
    fun provideActionTracker(trackingInteractor: TrackingInteractor): IActionsTracker
}