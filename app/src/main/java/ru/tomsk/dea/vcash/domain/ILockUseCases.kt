package ru.tomsk.dea.vcash.domain

import io.reactivex.Maybe
import io.reactivex.Single
import ru.tomsk.dea.vcash.domain.model.Wallet

interface ILockUseCases {
    val wallet: Maybe<Wallet>

    fun checkPin(pin: Int): Single<Boolean>
    fun onWalletCreation(pin: Int)
}