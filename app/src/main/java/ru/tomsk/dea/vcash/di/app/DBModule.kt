package ru.tomsk.dea.vcash.di.app

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.Single
import ru.tomsk.dea.vcash.data.db.WalletDatabase
import javax.inject.Singleton

/**
 * Created by DEA on 8/21/17.
 * Provides Single with DB instance
 */
@Module
class DBModule {
    @Singleton
    @Provides
    fun provideDBObservable(context: Context, scheduler: Scheduler)
            : Single<WalletDatabase> = Single.fromCallable {
        Room.databaseBuilder(context, WalletDatabase::class.java, "wallet_db").build()
    }.cache().observeOn(scheduler)
}