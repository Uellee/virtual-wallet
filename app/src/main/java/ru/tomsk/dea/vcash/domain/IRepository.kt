package ru.tomsk.dea.vcash.domain

import android.net.Uri
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import ru.tomsk.dea.vcash.domain.model.Cash
import ru.tomsk.dea.vcash.domain.model.Transaction
import ru.tomsk.dea.vcash.domain.model.Wallet

/**
 * Created by DEA on 8/14/17.
 * Repository methods, used by interactors
 */
interface IRepository {
    fun createTransaction(transaction: Transaction)
    fun createWallet(wallet: Wallet, hash: String, transactions: List<Transaction>? = null)
    fun lock(state: Boolean)
    fun import()
    fun clean()
    fun startExport(uri: Uri)
    fun checkKey(key: String)

    //    RX
    val isLocked: Observable<Boolean>
    val walletStream: Maybe<Wallet>
    val pinHashStream: Single<String>
    val transactionsStream: Observable<Transaction>
    val status: Observable<Cash>
    val keyCheckObservable: Observable<Boolean>
}