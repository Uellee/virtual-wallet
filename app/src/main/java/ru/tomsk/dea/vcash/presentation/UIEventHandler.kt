package ru.tomsk.dea.vcash.presentation

import android.content.Context
import android.widget.Toast
import javax.inject.Inject

/**
 * Created by DEA on 8/27/17.
 * Handles UI events
 */
class UIEventHandler @Inject constructor(val context: Context) {
    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}