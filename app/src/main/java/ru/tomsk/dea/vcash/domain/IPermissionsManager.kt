package ru.tomsk.dea.vcash.domain

import io.reactivex.Observable

interface IPermissionsManager {
    val WRITE_STORAGE_PERMISSION_GRANTED: Boolean

    fun queryWriteStoragePermission(): Observable<Int>
}