package ru.tomsk.dea.vcash.data

import android.content.Context
import android.util.Log
import ru.tomsk.dea.vcash.domain.model.Wallet

/**
 * Created by DEA on 8/14/17.
 * Provides access to shared preferences
 */
class WalletPreferences(val context: Context) {

    private val PREFERENCES_KEY = "preference_file_key"
    private val HASH_KEY = "hash_key"
    private val WALLET_KEY: String = "wallet_id_key"

    private val TAG = "#% WalletPreferences"

    private val sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE)

    fun getHash(): String = sharedPreferences.getString(HASH_KEY, "")

    fun getWalletId(): String? = sharedPreferences.getString(WALLET_KEY, null)

    fun putNewWallet(wallet: Wallet, walletHash: String) {
        sharedPreferences.edit()
                .putString(WALLET_KEY, wallet.id)
                .putString(HASH_KEY, walletHash)
                .apply()

        Log.d(TAG, "Putting new wallet: ${wallet.id}")
    }

    fun clean() {
        sharedPreferences.edit()
                .remove(WALLET_KEY)
                .remove(HASH_KEY)
                .apply()

        Log.d(TAG, "Removing data")
    }
}