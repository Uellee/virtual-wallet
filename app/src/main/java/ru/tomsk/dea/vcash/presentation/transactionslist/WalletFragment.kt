package ru.tomsk.dea.vcash.presentation.transactionslist


import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.databinding.FragmentWalletBinding
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.presentation.ViewModelFactory

class WalletFragment : LifecycleFragment() {

    private lateinit var fragmentActivity: AppCompatActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentWalletBinding>(
                inflater, R.layout.fragment_wallet, container, false)
        val factory = ViewModelFactory(ComponentOwner.uiComponent)

        val viewModel = ViewModelProviders.of(this, factory)
                .get(WalletViewModel::class.java)
        lifecycle.addObserver(viewModel)
        binding.vm = viewModel

        binding.toolbarVM = ViewModelProviders.of(this, factory)
                .get(ToolbarViewModel::class.java)

        val toolbar = binding.root.findViewById<Toolbar>(R.id.toolbar)
        fragmentActivity.setSupportActionBar(toolbar)
        fragmentActivity.supportActionBar?.title = null
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentActivity = activity as AppCompatActivity
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)
    }
}