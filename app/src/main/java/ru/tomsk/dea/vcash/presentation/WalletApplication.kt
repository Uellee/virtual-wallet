package ru.tomsk.dea.vcash.presentation

import android.app.Application
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.di.app.AppModule
import ru.tomsk.dea.vcash.di.app.DaggerAppComponent

class WalletApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        ComponentOwner.appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(baseContext)).build()

    }
}