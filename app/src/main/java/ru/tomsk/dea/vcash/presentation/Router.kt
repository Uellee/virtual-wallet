package ru.tomsk.dea.vcash.presentation

import android.support.v4.app.FragmentManager
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.domain.IRouter
import ru.tomsk.dea.vcash.presentation.outgoingtransaction.OutGoingTransactionFragment
import ru.tomsk.dea.vcash.presentation.pinlock.LockFragment
import ru.tomsk.dea.vcash.presentation.transactionslist.WalletFragment
import ru.tomsk.dea.vcash.presentation.walletgeneration.WalletGenerationFragment

/**
 * Created by DEA on 8/5/17.
 * Fragment manager
 */
class Router(val fragmentManager: FragmentManager) : IRouter {
    override fun stopCurrentFragment() {
        fragmentManager.popBackStack()
    }

    private val WALLET_FRAGMENT_TAG = "wallet_fragment"
    private val LOCK_FRAGMENT_TAG = "lock_fragment"
    private val GEN_WALLET_TAG = "generate_wallet_fragment"
    private val OUTGOING_TRANSACTION_FRAGMENT_TAG = "new_transaction_fragment_tag"

    override fun showWalletFragment() {
        val walletFragment = fragmentManager.findFragmentByTag(WALLET_FRAGMENT_TAG)
                ?: WalletFragment()

        fragmentManager
                .beginTransaction()
                .replace(R.id.fl_maincontent, walletFragment)
                .commit()

    }

    override fun showLockFragment() {
        val lockFragment = fragmentManager.findFragmentByTag(LOCK_FRAGMENT_TAG)
                ?: LockFragment()

        fragmentManager
                .beginTransaction()
                .replace(R.id.fl_maincontent, lockFragment)
                .commit()
    }

    override fun showGeneratePinFragment() {
        val genFragment = fragmentManager.findFragmentByTag(GEN_WALLET_TAG)
                ?: WalletGenerationFragment()

        fragmentManager
                .beginTransaction()
                .replace(R.id.fl_maincontent, genFragment)
                .commit()
    }

    override fun showNewTransactionFragment() {
        val outgoingTransactionFragment = fragmentManager.findFragmentByTag(OUTGOING_TRANSACTION_FRAGMENT_TAG)
                ?: OutGoingTransactionFragment()

        fragmentManager
                .beginTransaction().addToBackStack(OUTGOING_TRANSACTION_FRAGMENT_TAG)
                .replace(R.id.fl_maincontent, outgoingTransactionFragment)
                .commit()
    }
}