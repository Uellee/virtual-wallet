package ru.tomsk.dea.vcash.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface StatusDao {
    @Query("SELECT * FROM status")
    fun loadStatus(): Status

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(status: Status)

    @Query("DELETE FROM status")
    fun clearTable()
}