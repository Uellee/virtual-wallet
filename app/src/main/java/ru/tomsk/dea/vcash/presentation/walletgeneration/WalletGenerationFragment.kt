package ru.tomsk.dea.vcash.presentation.walletgeneration


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.tomsk.dea.vcash.R
import ru.tomsk.dea.vcash.databinding.FragmentWalletGenerationBinding
import ru.tomsk.dea.vcash.di.ComponentOwner
import ru.tomsk.dea.vcash.presentation.ViewModelFactory


/**
 * A simple [Fragment] subclass.
 */
class WalletGenerationFragment : Fragment() {
    override fun onSaveInstanceState(outState: Bundle?) {
//        Leads to a bug: java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
//        super.onSaveInstanceState(outState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentWalletGenerationBinding>(inflater, R.layout.fragment_wallet_generation, container, false)
        val factory = ViewModelFactory(ComponentOwner.uiComponent)

        binding.vm = ViewModelProviders.of(this, factory).get(WalletGenerationViewModel::class.java)

        return binding.root
    }

}// Required empty public constructor
