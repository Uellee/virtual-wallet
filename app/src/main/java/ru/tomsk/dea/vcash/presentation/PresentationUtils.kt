package ru.tomsk.dea.vcash.presentation

import android.content.Context
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.view.LayoutInflater

/**
 * Created by DEA on 7/30/17.
 * Utils
 */
val Context.layoutInflater: LayoutInflater
    get() = LayoutInflater.from(this)

infix fun <T> ObservableField<T>.set(value: T) {
    set(value)
}

infix fun ObservableBoolean.set(value: Boolean) {
    set(value)
}