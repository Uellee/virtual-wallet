package ru.tomsk.dea.vcash.domain

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import io.reactivex.Scheduler
import ru.tomsk.dea.vcash.presentation.WalletActivity
import javax.inject.Inject

/**
 * Created by DEA on 8/29/17.
 * Handles startExport process
 */
class ExportInteractor @Inject constructor(
        private val repository: IRepository,
        private val permissionsManager: IPermissionsManager,
        scheduler: Scheduler,
        private val context: Context
) : IExportUseCases {
    private val hashHandler = HashHandler()

    override val keyCheckObservable = repository.keyCheckObservable

    init {
        repository.keyCheckObservable
                .observeOn(scheduler)
                .filter { it }
                .subscribe {
                    startWalletActivity()
                }

    }

    private fun startWalletActivity() {
        val intent = Intent(context, WalletActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        context.startActivity(intent)
    }

    override fun startExport(uri: Uri) {
        if (permissionsManager.WRITE_STORAGE_PERMISSION_GRANTED)
            repository.startExport(uri)
        else {
            permissionsManager.queryWriteStoragePermission()
                    .map { it == PackageManager.PERMISSION_GRANTED }
                    .subscribe {
                        if (it) repository.startExport(uri)
                        else startWalletActivity() /* Start it as is */
                    }
        }
    }

    override fun restartExport(uri: Uri) {
        startExport(uri)
    }

    override fun checkKey(key: Int) {
        repository.checkKey(hashHandler.createHash(key))
    }
}
