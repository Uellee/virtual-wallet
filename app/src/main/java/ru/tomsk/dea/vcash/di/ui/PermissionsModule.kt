package ru.tomsk.dea.vcash.di.ui

import android.app.Activity
import dagger.Module
import dagger.Provides
import ru.tomsk.dea.vcash.domain.IPermissionsManager
import ru.tomsk.dea.vcash.presentation.PermissionsManager

@Module
class PermissionsModule(val activity: Activity) {
    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    fun providePermissionsManager(): IPermissionsManager = PermissionsManager(activity)
}